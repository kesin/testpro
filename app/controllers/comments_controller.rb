class CommentsController < ApplicationController
  def create
    params.permit!
    @post = Post.find (params[:post_id])
    @comment = @post.comments.create( params[:comment])
    redirect_to post_path(@post)
  end


  def destroy
    @post = Post.find (params[:post_id])
    @comment = @post.comments.find(params[:id])
    @comment.destroy
    redirect_to post_path(@post)
    end

end