module ApplicationHelper
  COLOR_SCHEMES = {
          1 => 'white',
          2 => 'black',
          3 => 'solarized-dark',
          4 => 'monokai',
        }
    COLOR_SCHEMES.default = 'white'

  def user_color_scheme_class
    COLOR_SCHEMES[current_user.try(:color_scheme_id)]
  end
end
